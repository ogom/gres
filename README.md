Gres
====

Git of reference in Scrum

## GitLab or GitHub

Story workflow of development.

## Application (GitLab or GitHub)

### Triangle is the relationship of reference.

* Issue
    * Issue Tracking
* Branch
    * Git Branch
* Request
    * Merge Request or Pull Request

## People

### Triangle is the relationship of scrum.

* Master
    * Scrum Master
* Engineer
    * Technical Engineer
* Reviewer
    * Technical Master

## Hexagonal

### Interactive People and Application

----

## Task Board

### Ready

* Issue from user.

### ToDo

* Create Issue to GitLab by Master.
* Assign Issue to Engineer.

### Doing

* See Issue by Engineer.
* Create Branch by Engineer.
* Testing by Engineer.
* Add evidence to Issue by Engineer.
* Push Branch by Engineer.
* Create Request to GitLab by Engineer.
* Assign code review to Reviewer.

### Review

* See Request by Reviewer.
* Make code review by Reviewer.
* Refactoring by Engineer.
* Add votes to Request by Reviewer.
* Assign Request to Master.

### Done

* Accept evidence to Issue by Master.

### Release

* Merge code by Master. And release.

----

## [Gitlab kanban](http://kanban.leanlabs.io/)

Simple kanban board for gitlab issues

### [Installation](http://kanban.leanlabs.io/documentation#installation)

#### Docker

```
$ brew install docker boot2docker fig
$ boot2docker init
$ boot2docker up
$ eval "$(boot2docker shellinit)"
```

#### Kanban

```
$ git clone https://github.com/leanlabsio/kanban.git
$ cd kanban/
$ edit fig.yml
$ fig up -d
```
